const originalEnvVars = Object.assign({}, process.env);

require('dotenv').config();

const sslcreds = require('./sslcreds.js');
const express = require('express');
const spdy = require('spdy');

const util = require('util');
const fs = require('fs');
const exec = util.promisify(require('child_process').exec);

const app = express();

async function command(cmd, cwd = null) {
  try {
    let { stdout, stderr } = await exec(cmd, { cwd, env: originalEnvVars });
    return stdout;
  } catch (err) {
    return '';
  }
}

(async () => {
  app.use(require('body-parser').json());

  app.use((req, res, next) => {
    if (req.headers['x-api-key'] !== process.env.DEPLOY_KEY && req.query.key !== process.env.DEPLOY_KEY) {
      return res.status(403).send('Forbidden.');
    }
    res.header({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers':
        'Origin, Content-Type, Accept, User-Agent, X-CSRF-Token, X-Requested-With, Authorization',
      'Access-Control-Allow-Credentials': 'true',
    });
    next();
  });

  app.route('/ping').get((req,res) => {
    res.send('Pong!');
  });

  app.route('/deploy').all(async (req,res) => {
    if (!req.query.env || !process.env[`SITE_${req.query.env}_APPNAME`] || !process.env[`SITE_${req.query.env}_HOSTNAME`]) {
      return res.status(400).send('Invalid request');
    }

    const appname = process.env[`SITE_${req.query.env}_APPNAME`];
    const hostname = process.env[`SITE_${req.query.env}_HOSTNAME`];
    const branch = process.env[`SITE_${req.query.env}_BRANCH`];
    const entrypoint = process.env[`SITE_${req.query.env}_ENTRYPOINT`];
    const cwd = `/var/www/${hostname}`;

    await command(`git remote update`, cwd);
    let gs = await command(`git status -uno`, cwd);

    if (gs.includes('branch is behind')) {
      let status = 'Deployed via webhook';

      const pre_stat_package_json = fs.statSync(`${cwd}/package.json`);
      await command(`git pull origin ${branch}`, cwd);
      const post_stat_package_json = fs.statSync(`${cwd}/package.json`);

      if (pre_stat_package_json.mtime.getTime() !== post_stat_package_json.mtime.getTime()) {
        // Only do npm install if package.json was modified by pull
        await command(`npm i`, cwd);
        status += ' (npm install complete)';
      } else {
        status += ' (npm install skipped)';
      }

      await command(`pm2 restart ${appname} --update-env`, cwd);
      return res.send(status);
    } else {
      return res.send('Already up-to-date');
    }
  });

  app.listen(process.env.HTTP_PORT, error => {
    if (error) {
      console.error(error)
      return process.exit(1)
    }
    console.log(`HTTP/2 Server running on port ${process.env.HTTP_PORT}`);
  });

  spdy.createServer(sslcreds, app).listen(process.env.HTTPS_PORT, error => {
    if (error) {
      console.error(error)
      return process.exit(1)
    }
    console.log(`HTTPS/2 Server running on port ${process.env.HTTPS_PORT}`);
  });
})();